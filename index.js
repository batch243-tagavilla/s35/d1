const express = require("express");
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});

// mongoose is a package that allows creation of schemas to model our data structures
// also has access to a number of methods for database manipulation
const mongoose = require("mongoose");

// [Section] MongoDB connection
// {newUrlParse:true} allows us to avoid any current and future errors while connectiong to MongoDB
mongoose.connect(
    "mongodb+srv://admin:admin@zuittbatch243.fvxpkh7.mongodb.net/?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

// set notifications for connection success or failure
let db = mongoose.connection;

// if a connection occured, output in the console
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Were connected to the cloud database"));

// [Section] Mongoose Schema
// schema determine the structure of the documents to be written in the database
// schema acts as a blueprint
// use the schema() constructor of the Mongoose module to create a new schema object
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending",
    },
});

// [Section] Models
// models use schemas and they act as the middleman from the server(JS code) to our database
// Server > Schema(blueprint) > Database > Collection
// follows the Model-View-Controller approach for naming convention
const Task = mongoose.model("Task", taskSchema);

// Create a POST route to create new task
// Business Logic
/*
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
		*/

app.post("/tasks", (req, res) => {
    // "findOne" is a mongoose method that acts similar to "find" in MongoDB
    Task.findOne({ name: req.body.name }, (err, result) => {
        // if a document was found, and the the document name matches the information sent via postman
        if (result != null && result.name == req.body.name) {
            return res.send("Duplicate Task Found");
        } else {
            let newTask = new Task({
                name: req.body.name,
            });

            // "save" method will store the information to the database
            newTask.save((saveErr, saveTask) => {
                //if there are errors in saving
                if (saveErr) {
                    return console.error(saveErr);
                }
                //No error found while creating the document
                else {
                    return res.status(200).send("new task created");
                }
            });
        }
    });
});

// Getting all the tasks
// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data:result
            });
        }
    });
});